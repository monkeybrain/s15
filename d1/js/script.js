
let favorite = "Takoyaki"
console.log(favorite);

let sum = (150 + 9)
console.log(sum);

let product = (100 * 90)
console.log(product);

let isActive = true;
console.log("isActive: " + isActive);

let faveResto = ['Jollibee', 'Mcdo', 'Biggs', 'Pabis', 'KFC']
console.log(faveResto);

let artist = {
	firstName: 'Francis', 
	lastName: 'Magalona',
	stageName:'The Mouth',
	birthDay:'October 4, 1964',
	age:'45',
	bestAlbum:'Rainy',
	bestSong:'3 Stars and a Sun',
	isActive: 'false'
}

console.log(artist);

let = quotient(10, 5);

function quotient(value1, value2) {
	division = value1 / value2;
	return division;
}

console.log(`The result of the division is: ${division}`);


// Mathematical operators
let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

// num1 = num1 +  num4;
num1 += num4;
console.log(num1);

// num2 = num2 + num4;
num2 += num4;
console.log(num2);

// num1 = num1 * 2;
num1 *= 2;
console.log(num1);

let string1 = "Boston ";
let string2 = " Celtics";

// string1 = string1 + string2;
string1 += string2;
console.log(string1);

// num1 = num1 - string1;
num1 -= string1;
console.log(num1);

let string3 = "Hello everyone";
let myArray = string3.split("", 3);
console.log(myArray);

// Mathematical operations - follows MDAS.

let mdasResult = 1 + 2 - 3 * 4 / 5;
/*
	3*4 = 12
	12/5 = 2.4
	1+2 =3
	3-2.4 = 0.6

 */
console.log(mdasResult);
// PEMDAS - Parenthesis, exponents, multiplication, division, addition and subtraction
let pemdasResult = 1 + (2-3) * (4/5);
/*
	4/5 = .8
	2-3 = -1
	-1*.8 = -0.8
	1+ -0.8 = .0
*/
console.log(pemdasResult);

// Increment and Decrement
// Two types Increment: Pre-fix and Post-fix

let z = 1;
//Pre-fix Incrementation
++z;
console.log(z);

// Post-fix Incrementation
z++;
console.log(z);
console.log(z++);
console.log(z);

// Pre-fix vs Post-fix Incrementation
console.log(z++);
console.log(z);

console.log(++z);

let n = 1;
console.log(++n);// 1 + n = 2
console.log(n);

console.log(n++);// n + 1 = 3
console.log(n);


// Pre-fix and Post-fix Decrementation
console.log(z);
console.log(z--);
console.log(z);

// comparison Operators - used to compare values
// Equality or Loose Equality Operator (==)
console.log(1 == 1);//true
console.log('1' == 1);

// strict equality
console.log(1 === 1);
console.log('1' === 1);

console.log('apple' == 'apple');
let isSame = 55 == 55;
console.log(isSame);

console.log(0 == false); // force coercion
console.log(1 == true);
console.log(true == 'true'); //1 != NaN

console.log(true == '1');
console.log('0' == false);

// Strict equality - checks both value and type
console.log(1 === '1');
console.log('Juan' === 'Juan')
console.log('Maria' === 'maria')

// Inequality Operators (!=)
	// Checks whether the operands are NOT equal and/or have different value
	// will do type coercion if the operands have different types:

	console.log('1' != 1);//false
	//false > both operands are converted to numbers
	//'1' converted into number is 1
	//1 converted into number is 1
	//1 == 1
	//not inequal
	
	console.log('James' != 'John')

	console.log(1 != "true");//true
	//with type conversion: true was converted to 1
	//"true" was convered into a number but results NaN
	//1 is not equal to NaN
	//it IS inequal

// strict inequality operator (!==) > it checks whether the two operand have different values and will check if they have different types

	console.log('5' !== 5)//true
	console.log(5 !== 5)//false

let name1 = 'Juan';
let name2 = 'Maria';
let name3 = 'Pedro';
let name4 = 'Perla';

let number1 = 50;
let number2 = 60;
let numString1 = "50";
let numString2 = "60";

console.log(numString1 == number1);
console.log(numString1 === number1);
console.log(numString1 != number1);
console.log(name4 !== name3);
console.log(name1 == 'juan');
console.log(name1 === "Juan");

// Relational Comparison Operators
	// A comparison operator - check the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;
let numString3 = "5500";

// Greater Than (>)
console.log( x > y);// false
console.log( w > y);

// Less Than (<)
console.log(w < y); //false
console.log(y >= y);//false
console.log(x < 1000);
console.log(numString3 < 1000);//false 
console.log(6000 < 'juan');//false

// Logical Operators
	// And Operator(&&) - both operands on the left and right or all operands added must be true or other it false
	// T && T = T
	// T && F = F
	// F && T = F
	// F && F = F

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1);//false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2);//true

let authorization3 = isAdmin && isLegalAge;
console.log(authorization3);//false

let requiredLevel = 95;
let requiredAge = 18;

let authorization4 = isRegistered && requiredLevel === 25;
console.log(authorization4);//false
let authorization5 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization5);

let userName1 = 'gamer';
let userName2 = 'shadowMaster';
let userAge1 = 15;
let userAge2 = 30;

let registration1 = userName1.length > 9 && userAge1 >= requiredAge;
//.length is a property of strings which determine the number of characters in the string
console.log(registration1);//false

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2);

let registration3 = userName1.length > 8 && userAge2 >=  requiredAge;
console.log(registration3);



/*let customerName = prompt("Enter your username:");
let password = prompt("Please enter password:")*/
/*if(customerName != null) {
document.getElementById("username").value = customerName;
}*/



/*console.log(`${customerName.length} ${password.length}`);*/


	
		

/*
function login(username, password){
	if(typeof username === "string" && typeof password === "string"){
		console.log("both arguments are string.");
} 	if (password.length >= 8 && customerName.length >= 8) {
			console.log(alert("Welcome!"))
		} else if (customerName.length < 8 ) {
			console.log(alert("username is too short"))
		} else if (password.length < 8 ) {
			console.log(alert("password is too short"))
} else {
	console.log(alert("Credentials too short."))
}		
}*/

//let day = prompt("Please enter the day of the week");

/*function colorOfTheDay(day) {
switch(day){
  case "Monday":
    alert(`today is ${day}. Wear black`);
    break;
  case "Tuesday":
    alert(`today is ${day}. Wear green`);
    break;
  case "Wednesday":
    alert(`today is ${day}. Wear yellow`);
    break;
  case "Thursday":
    alert(`today is ${day}. Wear red`);
    break;
  case "Friday":
    alert(`today is ${day}. Wear violet`);
    break; 
  case "Saturday":
    alert(`today is ${day}. Wear blue`);
    break;
  case "Sunday":
    alert(`today is ${day}. Wear white`);
    break;    
  default:
  	alert("Invalid input. Enter a valid day of the week")
    }
};

colorOfTheDay("Tuesday");*/

let grade = prompt("Please enter grade:");

function gradeEvaluator(grade) {
switch(true){
  case (grade >= 90):
    alert("A");
   break;
  case (grade >= 80):
    alert("B");
   break;
  case (grade >= 71):
     alert("C");
   break;
  case (grade <= 70):
    alert("F");
   break; 
  default:
    alert("Invalid");
    }
}; 

console.log(grade);